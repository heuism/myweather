package app.myweather;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.List;

import app.myweather.adapter.LocationAdapter;
import app.myweather.model.LocationData;
import app.myweather.network.NetworkRequest;
import app.myweather.parser.BaseParser;
import app.myweather.parser.LocationParser;
import app.myweather.pref.AppPreference;
import app.myweather.util.CachingUtil;
import app.myweather.util.VolleySingleton;

/**
 * Created by cuongmv162 on 11/4/2015.
 */
public class SearchActivity extends BaseActivity {

    private EditText mEditTextSearch;
    private ProgressBar mProgressBar;
    private ListView mListViewResult;
    private Button mButtonSearch;

    private View mViewPickLocation;
    private TextView mTextViewPickLocation;
    private TextView mTextViewMessage;
    private ImageView mImageViewDonePickLocation;

    private RequestQueue mRequestQueue;
    private LocationData mLocationData;

    public SearchActivity() {
        mActivityType = ActivityType.SEARCH_ACTIVITY;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mProgressBar = (ProgressBar) findViewById(R.id.fragment_search_progress);
        mListViewResult = (ListView) findViewById(R.id.fragment_search_result_list);
        mListViewResult.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mLocationData = (LocationData) parent.getItemAtPosition(position);
                displayPickedLocationView(true);
                mTextViewPickLocation.setText(mLocationData.getLocation().concat(", ").concat(mLocationData.getCountry()));
            }
        });

        mButtonSearch = (Button) findViewById(R.id.fragment_search_request_location);
        mButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(mEditTextSearch);
                doSearch();
            }
        });

        mViewPickLocation = (View) findViewById(R.id.view_pick_location);
        mTextViewPickLocation = (TextView) mViewPickLocation.findViewById(R.id.view_pick_location_location);
        mTextViewMessage = (TextView)mViewPickLocation.findViewById(R.id.view_pick_location_location_text);
        mImageViewDonePickLocation = (ImageView) mViewPickLocation.findViewById(R.id.view_pick_location_done);
        mImageViewDonePickLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppPreference.setCurrentPickedLocation(SearchActivity.this, mLocationData.getLocation().concat(", ").concat(mLocationData.getCountry()));
                CachingUtil.cacheLocation(SearchActivity.this, mLocationData);
                displayPickedLocationView(false);
                finish();

            }
        });
        mEditTextSearch = (EditText) findViewById(R.id.fragment_search_search_location);
        mEditTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                List<LocationData> list = CachingUtil.getCachedLocationList(SearchActivity.this);
                if(list != null) {
                    LocationAdapter locationAdapter = new LocationAdapter(SearchActivity.this, list);
                    mListViewResult.setAdapter(locationAdapter);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mRequestQueue = VolleySingleton.getInstance().getRequestQueue();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action_close:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void doSearch() {
        if (TextUtils.isEmpty(mEditTextSearch.getText().toString())) {
            return;
        }
        enableSearch(false);
        displayProgress(true);
        displayPickedLocationView(false);
        mListViewResult.setAdapter(null);
        try {
            String url = NetworkRequest.getRequest(NetworkRequest.RequestType.LOCATION_REQUEST, NetworkRequest.ApiKeyType.NORMAL_KEY, mEditTextSearch.getText().toString());
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("SearchActivity", "doSearch onResponse");
                    Log.d("SearchActivity", "response " + response);

                    List<LocationData> list = null;
                    try {
                        list = LocationParser.getLocationList(response);
                        if(list !=null) {
                            LocationAdapter locationAdapter = new LocationAdapter(SearchActivity.this, list);
                            mListViewResult.setAdapter(locationAdapter);
                            locationAdapter.notifyDataSetChanged();
                            displayPickLocation(true);
                            mTextViewMessage.setText(R.string.you_picked_location);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("SearchActivity", "JSONException " + e.getMessage());
                        String message = BaseParser.noMatchedData(response);
                        displayPickedLocationView(true);
                        mTextViewMessage.setText(R.string.message);
                        mTextViewPickLocation.setText(message);
                        displayPickLocation(false);
                    }

                    displayProgress(false);
                    enableSearch(true);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    displayProgress(false);
                    displayPickedLocationView(false);
                    enableSearch(true);
                }
            });

            mRequestQueue.add(stringRequest);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void displayPickLocation(boolean display) {
        if(display){
            mImageViewDonePickLocation.setVisibility(View.VISIBLE);
        }else{
            mImageViewDonePickLocation.setVisibility(View.INVISIBLE);
        }

    }

    private void displayProgress(boolean display) {
        if (display) {
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void displayPickedLocationView(boolean display) {
        if (display) {
            mViewPickLocation.setVisibility(View.VISIBLE);
        } else {
            mViewPickLocation.setVisibility(View.GONE);
        }
    }

    private void enableSearch(boolean enable) {
        if (enable) {
            mButtonSearch.setEnabled(true);
            mButtonSearch.setClickable(true);
        } else {
            mButtonSearch.setEnabled(false);
            mButtonSearch.setClickable(false);
        }
    }
}
