package app.myweather.pref;

import android.content.Context;

import app.myweather.util.SharedPreferenceHelper;

/**
 * Created by cuongmv162 on 11/4/2015.
 */
public class AppPreference {
    private static final String APP_PREFERENCE = "MyWeatherPref";
    private static final String CURRENT_PICKED_LOCATION = "CURRENT_PICKED_LOCATION";
    private static final String LOCATION_LIST = "LOCATION_LIST";
    private static final String WEATHER_LIST = "WEATHER_LIST";

    public static void setCurrentPickedLocation(Context context, String location){
        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(context);
        sharedPreferenceHelper.setPreferenceName(APP_PREFERENCE);
        sharedPreferenceHelper.setKey(CURRENT_PICKED_LOCATION);
        sharedPreferenceHelper.setValue(location);
        sharedPreferenceHelper.addPreference();
    }

    public static String getCurrentPickedLocation(Context context){
        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(context);
        sharedPreferenceHelper.setPreferenceName(APP_PREFERENCE);
        return (String) sharedPreferenceHelper.getPreference(CURRENT_PICKED_LOCATION, String.class);
    }

    public static void updateLocationList(Context context, String rawData){
        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(context);
        sharedPreferenceHelper.setPreferenceName(APP_PREFERENCE);
        sharedPreferenceHelper.setKey(LOCATION_LIST);
        sharedPreferenceHelper.setValue(rawData);
        sharedPreferenceHelper.addPreference();
    }

    public static String getLocationList(Context context){
        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(context);
        sharedPreferenceHelper.setPreferenceName(APP_PREFERENCE);
        return (String) sharedPreferenceHelper.getPreference(LOCATION_LIST, String.class);
    }

    public static void updateWeatherList(Context context, String rawData){
        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(context);
        sharedPreferenceHelper.setPreferenceName(APP_PREFERENCE);
        sharedPreferenceHelper.setKey(WEATHER_LIST);
        sharedPreferenceHelper.setValue(rawData);
        sharedPreferenceHelper.addPreference();
    }

    public static String getWeatherList(Context context){
        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(context);
        sharedPreferenceHelper.setPreferenceName(APP_PREFERENCE);
        return (String) sharedPreferenceHelper.getPreference(WEATHER_LIST, String.class);
    }
}
