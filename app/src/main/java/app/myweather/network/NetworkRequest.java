package app.myweather.network;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by cuongmv162 on 11/3/2015.
 */
public class NetworkRequest {

    public enum ApiKeyType {
        NORMAL_KEY(1, "dd5bc7742928d1633213b5da075ec&q", "NORMAL_KEY"),
        AWESOME_KEY(2, "f16c45e7217c8641fa4d5ae8e2415", "AWESOME_KEY");

        private int id;
        private String key;
        private String name;

        ApiKeyType(int id, String key, String name) {
            this.id = id;
            this.key = key;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public String getApiKey() {
            return key;
        }

        public String getName() {
            return name;
        }
    }

    public enum RequestType {
        WEATHER_REQUEST(1, "WEATHER_REQUEST"),
        LOCATION_REQUEST(2, "LOCATION_REQUEST");

        private int id;
        private String name;

        RequestType(int id,String name) {
            this.id = id;

            this.name = name;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    private static final String WEATHER_API = "http://api.worldweatheronline.com/free/v2/weather.ashx?";
    private static final String LOCATION_API = "http://api.worldweatheronline.com/free/v2/search.ashx?";
    private static final String DATA_JSON_FORMAT = "format=json";
    private static final String QUERY = "query=";
    private static final String KEY = "key=";

    public static String getRequest(RequestType requestType, ApiKeyType apiKey, String query) throws UnsupportedEncodingException {
        String url = null;
        switch (requestType){
            case WEATHER_REQUEST:
                url = getWeather(query, apiKey);
                break;
            case LOCATION_REQUEST:
                url = getLocation(query, apiKey);
                break;
            default:
                break;
        }
        return url;
    }

    private static String getWeather(String location, ApiKeyType apiKey) throws UnsupportedEncodingException {
        String url = WEATHER_API;
        String encodedQuery = URLEncoder.encode(location,"UTF-8");
        url = url.concat(QUERY).concat(encodedQuery);
        switch (apiKey) {
            case AWESOME_KEY:
                url = url.concat("&").concat(KEY).concat(ApiKeyType.AWESOME_KEY.getApiKey()).concat("&").concat(DATA_JSON_FORMAT);
                break;
            case NORMAL_KEY:
                url = url.concat("&").concat(KEY).concat(ApiKeyType.NORMAL_KEY.getApiKey()).concat("&").concat(DATA_JSON_FORMAT);
                break;
            default:
                break;
        }

        return url;
    }

    private static String getLocation(String location, ApiKeyType apiKey) throws UnsupportedEncodingException {
        String url = LOCATION_API;
        String encodedQuery = URLEncoder.encode(location,"UTF-8");
        url = url.concat(QUERY).concat(encodedQuery);
        switch (apiKey) {
            case AWESOME_KEY:
                url = url.concat("&").concat(KEY).concat(ApiKeyType.AWESOME_KEY.getApiKey()).concat("&").concat(DATA_JSON_FORMAT);
                break;
            case NORMAL_KEY:
                url = url.concat("&").concat(KEY).concat(ApiKeyType.NORMAL_KEY.getApiKey()).concat("&").concat(DATA_JSON_FORMAT);
                break;
            default:
                break;
        }

        return url;
    }
}
