package app.myweather;

import android.app.Application;
import android.content.Context;

/**
 * Created by cuongmv162 on 11/3/2015.
 */
public class WeatherApplication extends Application {
    private static WeatherApplication application;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        application = this;

        this.setAppContext(getApplicationContext());
    }

    public static WeatherApplication getInstance(){
        return application;
    }
    public static Context getAppContext() {
        return context;
    }
    public void setAppContext(Context mAppContext) {
        this.context = mAppContext;
    }

}
