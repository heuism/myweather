package app.myweather;

import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import app.myweather.fragment.BaseFragment;
import app.myweather.fragment.DetailFragment;
import app.myweather.util.IntentUtil;

public class MainActivity extends BaseActivity {
    private DetailFragment detailFragment;

    public MainActivity() {
        mActivityType = ActivityType.MAIN_ACTIVITY;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        openDetailFragment();
    }

    public void openDetailFragment() {
        Log.d("MainActivity", "openDetailFragment");
        detailFragment = new DetailFragment();
        openFragment(detailFragment, R.id.activity_main_content, BaseFragment.FragmentType.FRAGMENT_DETAIL);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_action_add_location:
                startActivityForResult(IntentUtil.createIntent(MainActivity.this, SearchActivity.class, OPEN_SEARCH_ACTIVITY), OPEN_SEARCH_ACTIVITY_REQUEST_CODE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


}
