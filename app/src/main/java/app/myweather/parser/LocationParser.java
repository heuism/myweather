package app.myweather.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.myweather.model.LocationData;

import java.util.List;
import java.util.ArrayList;

/**
 * Created by cuongmv162 on 11/3/2015.
 */
public class LocationParser extends BaseParser {
    private static final String SEARCH_API = "search_api";
    private static final String RESULT = "result";
    private static final String AREA_NAME = "areaName";
    private static final String COUNTRY = "country";
    private static final String WEATHER_URL = "weatherUrl";

    public static List<LocationData> getLocationList(String json) throws JSONException {
        List<LocationData> list = new ArrayList<LocationData>();
        LocationData locationData = null;
        JSONObject jsonObject = new JSONObject(json);
        JSONObject searchApi = jsonObject.getJSONObject(SEARCH_API);
        JSONArray result = searchApi.getJSONArray(RESULT);
        for (int i = 0; i < result.length(); i++) {
            JSONObject locationObject = result.getJSONObject(i);
            locationData = new LocationData();
            JSONArray areaName = locationObject.getJSONArray(AREA_NAME);
            for(int a = 0; a < areaName.length(); a++){
                JSONObject areaNameObject = areaName.getJSONObject(a);
                locationData.setLocation(areaNameObject.getString(VALUE));
            }
            JSONArray countryArray = locationObject.getJSONArray(COUNTRY);
            for(int c =0 ; c<countryArray.length();c++){
                JSONObject countryObject = countryArray.getJSONObject(c);
                locationData.setCountry(countryObject.getString(VALUE));

            }
            locationData.setWeatherUrl(locationObject.getString(WEATHER_URL));
            list.add(locationData);
        }
        return list;
    }
}
