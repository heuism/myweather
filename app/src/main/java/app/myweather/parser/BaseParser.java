package app.myweather.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by cuongmv162 on 11/4/2015.
 */
public class BaseParser {
    public static final String DATA = "data";
    public static final String VALUE = "value";
    public static final String ERROR = "error";
    public static final String MESSAGE = "msg";

    public static String noMatchedData(String json) {
        String message = null;
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            JSONObject data =jsonObject.getJSONObject(DATA);
            JSONArray errorJsonArray = data.getJSONArray(ERROR);
            for (int i = 0; i <= errorJsonArray.length(); i++) {
                JSONObject messageObject = errorJsonArray.getJSONObject(i);
                message = messageObject.getString(MESSAGE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return message;
    }
}
