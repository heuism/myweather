package app.myweather.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.myweather.model.LocationData;
import app.myweather.model.WeatherData;

/**
 * Created by cuongmv162 on 11/3/2015.
 */
public class WeatherParser extends BaseParser{
    private static final String CURRENT_CONDITION = "current_condition";
    private static final String HUMIDITY = "humidity";
    private static final String OBSERVATION_TIME = "observation_time";
    private static final String TEMP_C = "temp_C";
    private static final String WEATHER_DESCRIPTION = "weatherDesc";
    private static final String WEATHER_ICON_URL = "weatherIconUrl";
    private static final String REQUEST = "request";
    private static final String QUERY = "query";
    private static final String MAX_TEMP_C = "maxtempC";
    private static final String MIN_TEMP_C = "mintempC";
    private static final String WEATHER = "weather";

    public static WeatherData getWeather(String json) throws JSONException {

        WeatherData weatherData = new WeatherData();
        LocationData locationData = new LocationData();
        weatherData.setLocationData(locationData);

        JSONObject jsonObject = new JSONObject(json);
        JSONObject data = jsonObject.getJSONObject(DATA);
        JSONArray currentCondition = data.getJSONArray(CURRENT_CONDITION);
        for (int i = 0; i < currentCondition.length(); i++) {
            JSONObject currentConditionObject = currentCondition.getJSONObject(i);
            weatherData.setObservationTime(currentConditionObject.getString(OBSERVATION_TIME));
            weatherData.setHumidity(currentConditionObject.getInt(HUMIDITY));
            weatherData.setTemperature(currentConditionObject.getInt(TEMP_C));
            JSONArray weatherDescription = currentConditionObject.getJSONArray(WEATHER_DESCRIPTION);
            for (int j = 0; j < weatherDescription.length(); j++) {
                JSONObject descriptionObject = weatherDescription.getJSONObject(j);
                weatherData.setDescription(descriptionObject.getString(VALUE));
            }
            JSONArray weatherUrl = currentConditionObject.getJSONArray(WEATHER_ICON_URL);
            for (int j = 0; j < weatherUrl.length(); j++) {
                JSONObject weatherUrlObject = weatherUrl.getJSONObject(j);
                weatherData.setWeatherIconUrl(weatherUrlObject.getString(VALUE));
            }

        }
        JSONArray location = data.getJSONArray(REQUEST);
        for (int l = 0; l < location.length(); l++) {
            JSONObject requestObject = location.getJSONObject(l);
            weatherData.getLocationData().setLocation(requestObject.getString(QUERY));
        }
        JSONArray weather = data.getJSONArray(WEATHER);
        for (int w = 0; w < weather.length(); w++) {
            JSONObject weatherObject = weather.getJSONObject(w);
            weatherData.setMaxTemperature(weatherObject.getInt(MAX_TEMP_C));
            weatherData.setMinTemperature(weatherObject.getInt(MIN_TEMP_C));
        }

        return weatherData;
    }
}
