package app.myweather.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import app.myweather.R;
import app.myweather.model.LocationData;

/**
 * Created by cuongmv162 on 11/3/2015.
 */
public class LocationAdapter extends BaseAdapter {
    private static final int LAYOUT_ID = R.layout.adapter_location;
    private Activity mActivity;
    private List<LocationData> mList;

    public LocationAdapter(Activity context, List<LocationData> list) {
        this.mActivity = context;
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mList.indexOf(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout row;
        ViewHolder viewHolder = null;

        if(convertView == null){
            LayoutInflater inflater = mActivity.getLayoutInflater();
            row = (LinearLayout) inflater.inflate(LAYOUT_ID, null);
            viewHolder = new ViewHolder();
            viewHolder.mTextViewLocation = (TextView)row.findViewById(R.id.adapter_location_name);
            row.setTag(viewHolder);
        } else {
            row = (LinearLayout)convertView;
            viewHolder = (ViewHolder)row.getTag();
        }

        LocationData locationData= (LocationData) getItem(position);
        viewHolder.mTextViewLocation.setText(locationData.getLocation().concat(", ").concat(locationData.getCountry()));

        return row;
    }

    private class ViewHolder{
        TextView mTextViewLocation;
    }
}
