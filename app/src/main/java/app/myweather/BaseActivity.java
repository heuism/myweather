package app.myweather;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import app.myweather.fragment.BaseFragment;

/**
 * Created by cuongmv162 on 11/3/2015.
 */
public class BaseActivity extends AppCompatActivity {
    public static final String OPEN_SEARCH_ACTIVITY = "OPEN_SEARCH_ACTIVITY";
    public static final int OPEN_SEARCH_ACTIVITY_REQUEST_CODE = 162;

    public enum ActivityType{
        MAIN_ACTIVITY(1, R.layout.activity_main, "MAIN_ACTIVITY"),
        SPLASH_ACTIVITY(2, R.layout.activity_splash, "SPLASH_ACTIVITY"),
        SEARCH_ACTIVITY(3, R.layout.activity_search, "SEARCH_ACTIVITY");

        private int id;
        private int layoutId;
        private String name;

        ActivityType(int id, int layoutId, String name){
            this.id = id;
            this.layoutId = layoutId;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLayoutId() {
            return layoutId;
        }

        public void setLayoutId(int layoutId) {
            this.layoutId = layoutId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    protected ActivityType mActivityType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(mActivityType.getLayoutId());
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
    }

    public void openFragment(BaseFragment baseFragment, int contentLayoutId, BaseFragment.FragmentType fragmentType){
        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(contentLayoutId, baseFragment, fragmentType.getName());
        fragmentTransaction.commit();
    }

    public void displayProgress(){

    }

    public void displayToast(Activity activity, String message){
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    public void hideSoftKeyboard(EditText input) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
    }
}
