package app.myweather.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;

import app.myweather.BaseActivity;
import app.myweather.MainActivity;
import app.myweather.R;
import app.myweather.SearchActivity;
import app.myweather.model.LocationData;
import app.myweather.model.WeatherData;
import app.myweather.network.NetworkRequest;
import app.myweather.parser.WeatherParser;
import app.myweather.pref.AppPreference;
import app.myweather.util.CachingUtil;
import app.myweather.util.IntentUtil;
import app.myweather.util.NetworkUtil;
import app.myweather.util.VolleySingleton;
import de.hdodenhof.circleimageview.CircleImageView;

import java.util.List;
import java.util.ArrayList;

/**
 * Created by cuongmv162 on 11/3/2015.
 */
public class DetailFragment extends BaseFragment {
    private Context mContext;

    private TextView mTextViewDescription;
    private TextView mTextViewTemperature;
    private TextView mTextViewLocation;
    private TextView mTextViewHumidity;
    private TextView mTextViewMaxTemperature;
    private TextView mTextViewMinTemperature;
    private TextView mTextViewObservationTime;
    private CircleImageView mImageViewWeatherIcon;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private RequestQueue mRequestQueue;

    public DetailFragment() {
        mFragmentType = FragmentType.FRAGMENT_DETAIL;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        mTextViewDescription = (TextView) view.findViewById(R.id.fragment_detail_description);
        mTextViewHumidity = (TextView) view.findViewById(R.id.fragment_detail_humidity);
        mTextViewTemperature = (TextView) view.findViewById(R.id.fragment_detail_temperature);
        mTextViewLocation = (TextView) view.findViewById(R.id.fragment_detail_location);
        mTextViewMaxTemperature = (TextView) view.findViewById(R.id.fragment_detail_max_temperature);
        mTextViewMinTemperature = (TextView) view.findViewById(R.id.fragment_detail_min_temperature);
        mTextViewObservationTime = (TextView) view.findViewById(R.id.fragment_detail_observation_time);
        mImageViewWeatherIcon = (CircleImageView) view.findViewById(R.id.fragment_detail_weather_icon);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.fragment_detail_swipeable_container);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.d("DetailFragment", "mSwipeRefreshLayout.setOnRefreshListener onRefresh");
                getWeatherDataWithNormalApi();
            }
        });

        mRequestQueue = VolleySingleton.getInstance().getRequestQueue();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context != null) {
            if (context instanceof MainActivity) {
                mContext = context;
            }
        } else {
            mContext = getActivity();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("DetailFragment", "requestCode " + requestCode);
        if (requestCode == BaseActivity.OPEN_SEARCH_ACTIVITY_REQUEST_CODE) {
            getWeatherDataWithNormalApi();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(NetworkUtil.getConnectivityStatus(mContext)==NetworkUtil.TYPE_NOT_CONNECTED){
            getWeatherDataFromCached();
        }else{
            getWeatherDataWithNormalApi();
        }
    }

    private void getWeatherDataFromCached(){
        String location = AppPreference.getCurrentPickedLocation(mContext);
        WeatherData weatherData = CachingUtil.getWeatherData(mContext, location);
        if(weatherData!=null){
            populateUi(weatherData);
        }
    }

    private void getWeatherDataWithNormalApi() {
        String location = AppPreference.getCurrentPickedLocation(mContext);
        if (TextUtils.isEmpty(location)) {
            startActivity(IntentUtil.createIntent((MainActivity) mContext, SearchActivity.class, BaseActivity.OPEN_SEARCH_ACTIVITY));
            return;
        }

        try {
            String url = NetworkRequest.getRequest(NetworkRequest.RequestType.WEATHER_REQUEST, NetworkRequest.ApiKeyType.NORMAL_KEY, location);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("DetailFragment", "onResponse ");
                    Log.d("DetailFragment", "response " + response);
                    displayProgres(false);
                    try {
                        WeatherData weatherData = null;
                        weatherData = WeatherParser.getWeather(response);
                        populateUi(weatherData);
                        CachingUtil.cacheWeather(mContext,weatherData);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    displayProgres(false);
                }
            });

            mRequestQueue.add(stringRequest);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void populateUi(WeatherData weatherData) {
        mTextViewDescription.setText(weatherData.getDescription());
        mTextViewHumidity.setText("humidity ".concat(String.valueOf(weatherData.getHumidity())));
        mTextViewObservationTime.setText(weatherData.getObservationTime());
        mTextViewMaxTemperature.setText("max temperature ".concat(String.valueOf(weatherData.getMaxTemperature())));
        mTextViewMinTemperature.setText("min temperature ".concat(String.valueOf(weatherData.getMinTemperature())));
        mTextViewLocation.setText(weatherData.getLocationData().getLocation());
        mTextViewTemperature.setText(String.valueOf(weatherData.getTemperature()));
        Picasso.with(mContext).load(weatherData.getWeatherIconUrl()).into(mImageViewWeatherIcon);
    }

    private void displayProgres(boolean display){
        if(display){
            mSwipeRefreshLayout.setRefreshing(true);
        }else{
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }
}
