package app.myweather.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.myweather.R;

/**
 * Created by cuongmv162 on 11/3/2015.
 */
public class BaseFragment extends Fragment {
    public enum FragmentType {
        FRAGMENT_DETAIL(1, R.layout.fragment_detail, "FRAGMENT_DETAIL");

        private int id;
        private int layoutId;
        private String name;

        FragmentType(int id, int layoutId, String name) {
            this.id = id;
            this.layoutId = layoutId;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getLayoutId() {
            return layoutId;
        }

        public void setLayoutId(int layoutId) {
            this.layoutId = layoutId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    protected Context mContext;
    protected FragmentType mFragmentType;
    protected View mContainer;
    public BaseFragment() {
    }

    @SuppressLint("ValidFragment")
    public BaseFragment(Context context) {
        this.mContext = context;
    }

    @SuppressLint("ValidFragment")
    public BaseFragment(Context context, FragmentType fragmentType) {
        this.mContext = context;
        this.mFragmentType = fragmentType;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("BaseFragment", "onCreateView");
        Log.d("BaseFragment", "mFragmentType " + mFragmentType.getName());

        mContainer = inflater.inflate(mFragmentType.getLayoutId(), null);

        Log.d("BaseFragment", "mContainer " + mContainer);

        return mContainer;
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }
}
