package app.myweather.util;

public interface ISharedPreferenceHelper<T> {
	public T getValue();
    public void setValue(T value);
    public void setKey(String name);
}
