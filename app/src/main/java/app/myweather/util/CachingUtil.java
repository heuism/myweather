package app.myweather.util;

import android.content.Context;
import android.location.Location;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import app.myweather.model.LocationData;
import app.myweather.model.WeatherData;
import app.myweather.pref.AppPreference;

import java.lang.reflect.Type;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

/**
 * Created by cuongmv162 on 11/3/2015.
 */
public class CachingUtil {
    private static final int CACHED_MAXIMUM = 10;

    public static WeatherData getWeatherData(Context context, String location) {
        List<WeatherData> list = getCachedWeatherList(context);
        WeatherData weatherData = null;
        if (list == null) {
            return null;
        }
        for (WeatherData w : list) {
            if (w.getLocationData().getLocation().equals(location)) {
                weatherData = new WeatherData();
                weatherData.setLocationData(w.getLocationData());
                weatherData.setMaxTemperature(w.getMaxTemperature());
                weatherData.setMinTemperature(w.getMinTemperature());
                weatherData.setDescription(w.getDescription());
                weatherData.setHumidity(w.getHumidity());
                weatherData.setWeatherIconUrl(w.getWeatherIconUrl());
                weatherData.setObservationTime(w.getObservationTime());
                weatherData.setTemperature(w.getTemperature());
                break;
            }

        }
        return weatherData;
    }

    public static void cacheWeather(Context context, WeatherData locationData) {
        List<WeatherData> list = getCachedWeatherList(context);
        if (list == null) {
            list = new ArrayList<WeatherData>();
            list.add(locationData);
            updateWeatherList(context, list);
        } else if (list.size() <= CACHED_MAXIMUM) {
            if (!list.contains(locationData)) {
                list.add(locationData);
                updateWeatherList(context, list);
            }
        }
    }

    public static void cacheLocation(Context context, LocationData locationData) {
        List<LocationData> list = getCachedLocationList(context);
        if (list == null) {
            list = new ArrayList<LocationData>();
            list.add(locationData);
            updateLocationList(context, list);
        } else if (list.size() <= CACHED_MAXIMUM) {
            list.add(locationData);
            int count = list.size();

            for (int i = 0; i < count; i++) {
                for (int j = i + 1; j < count; j++) {
                    if (list.get(i).getLocation().equals(list.get(j).getLocation()) & list.get(i).getWeatherUrl().equals(list.get(j).getWeatherUrl())) {
                        list.remove(j--);
                        count--;
                    }
                }
            }
            updateLocationList(context, list);
        }
    }

    public static List<WeatherData> getCachedWeatherList(Context context) {
        Gson gson = new Gson();
        String rawJson = AppPreference.getWeatherList(context);
        Type type = new TypeToken<List<WeatherData>>() {
        }.getType();
        return gson.fromJson(rawJson, type);
    }

    public static void updateWeatherList(Context context, List<WeatherData> list) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<WeatherData>>() {
        }.getType();
        String jsonData = gson.toJson(list, type);
        AppPreference.updateWeatherList(context, jsonData);
    }

    public static List<LocationData> getCachedLocationList(Context context) {
        Gson gson = new Gson();
        String rawJson = AppPreference.getLocationList(context);
        Type type = new TypeToken<List<LocationData>>() {
        }.getType();
        return gson.fromJson(rawJson, type);
    }

    public static void updateLocationList(Context context, List<LocationData> list) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<LocationData>>() {
        }.getType();
        String jsonData = gson.toJson(list, type);
        AppPreference.updateLocationList(context, jsonData);
    }
}
