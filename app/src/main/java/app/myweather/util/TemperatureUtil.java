package app.myweather.util;

public class TemperatureUtil {
	
	public enum MetricSystem{
		METRIC, IMPERIAL;
	}
	
	public static double getFahrenheit(double celcius) {
		double result;

		result = celcius * 9 / 5 + 32;

		return result;
	}

	public static double getCelcius(double fahrenheit) {
		double result;

		result = (fahrenheit - 32) * 5 / 9;

		return result;
	}
	
	
}
